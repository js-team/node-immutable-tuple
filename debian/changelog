node-immutable-tuple (0.4.10-11) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + node-immutable-tuple: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 01 Dec 2022 23:44:59 +0000

node-immutable-tuple (0.4.10-10) unstable; urgency=medium

  * Team upload
  * Update lintian overrides
  * Replace embedded minipass/minizlib by test dependencies
  * Remove unused license definitions for ISC.
  * Update standards version to 4.6.1, no changes needed.
  * Add fix for rollup 3 (Closes: #1022641)

 -- Yadd <yadd@debian.org>  Wed, 26 Oct 2022 18:14:23 +0200

node-immutable-tuple (0.4.10-9) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright)
  * Remove constraints unnecessary since stretch
  * Remove constraints unnecessary since buster

  [ Yadd ]
  * Bump debhelper compatibility level to 13
  * Modernize debian/watch
    * Fix GitHub tags regex
    * Fix filenamemangle
    * Use /tags instead of /releases
  * Use dh-sequence-nodejs
  * Update standards version to 4.6.0, no changes needed.
  * Drop dependency to nodejs

 -- Yadd <yadd@debian.org>  Sun, 21 Nov 2021 13:54:45 +0100

node-immutable-tuple (0.4.10-8) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Team upload.

  [ Olivier Tilloy ]
  * Remove an obsolete test that fails with recent versions of Node.js
    (Closes: #963771)

 -- Olivier Tilloy <olivier.tilloy@canonical.com>  Tue, 14 Jul 2020 18:09:02 +0200

node-immutable-tuple (0.4.10-7) unstable; urgency=medium

  * Team upload
  * Fix test with pkg-js-tools ≥ 0.9.33 (Closes: #959560)

 -- Xavier Guimard <yadd@debian.org>  Sun, 03 May 2020 16:40:29 +0200

node-immutable-tuple (0.4.10-6) unstable; urgency=medium

  * Team upload
  * Back to unstable (Closes: #954719)

 -- Xavier Guimard <yadd@debian.org>  Sun, 22 Mar 2020 20:20:04 +0100

node-immutable-tuple (0.4.10-5) experimental; urgency=medium

  * Team upload
  * Fix test for mocha ≥ 7

 -- Xavier Guimard <yadd@debian.org>  Fri, 21 Feb 2020 00:18:10 +0100

node-immutable-tuple (0.4.10-4) experimental; urgency=medium

  * Team upload

  [ Pirate Praveen ]
  * Update build dependencies to build with rollup 1.0

  [ Xavier Guimard ]
  * Declare compliance with policy 4.5.0
  * Add "Rules-Requires-Root: no"
  * Update test embedded modules

 -- Xavier Guimard <yadd@debian.org>  Sun, 26 Jan 2020 15:13:04 +0100

node-immutable-tuple (0.4.10-3) unstable; urgency=medium

  * Team upload
  * Add node-rollup-plugin-buble as build dependency (rollup dropped this
    dependency from 0.56.3-1)

 -- Pirate Praveen <praveen@debian.org>  Mon, 30 Sep 2019 21:42:19 +0530

node-immutable-tuple (0.4.10-2) unstable; urgency=medium

  * Team upload
  * Bump debhelper compatibility level to 12
  * Declare compliance with policy 4.4.0
  * Add debian/gbp.conf
  * Add upstream/metadata
  * Add patch description
  * Switch install and minimal test to pkg-js-tools
  * Enable upstream test. This embeds reify
  * Add lintian overrides

 -- Xavier Guimard <yadd@debian.org>  Wed, 07 Aug 2019 07:23:33 +0200

node-immutable-tuple (0.4.10-1) unstable; urgency=low

  [ Deva sena priya, Jeya Sree ]
  * Initial release (Closes: #926075)

 -- Jeya Sree <jeyasreepriya@gmail.com>  Sun, 31 Mar 2019 17:52:01 +0530
